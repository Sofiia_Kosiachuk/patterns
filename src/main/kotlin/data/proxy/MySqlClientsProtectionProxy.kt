package data.proxy

import data.dao.ClientsDao
import data.factory.impl.MySqlClientsDaoFactory
import domain.entity.users.User
import domain.entity.users.UserRole
import kotlin.system.measureTimeMillis

private data class Key(
    val id: Int,
    val offset: Int,
    val rowCount: Int
)

class MySqlClientsProtectionProxy : ClientsDao {

    private val clientsDao: ClientsDao = MySqlClientsDaoFactory().create()

    private val cache = mutableMapOf<Key, List<User>>()

    override suspend fun getById(clientId: Int) = clientsDao.getById(clientId)

    override suspend fun getByTrainerId(trainerId: Int, offset: Int, rowCount: Int): List<User> {
        val key = Key(trainerId, offset, rowCount)

        return if (cache.containsKey(key)) {
            val users: List<User>
            val time = measureTimeMillis { users = cache[key]!! }
            println("Getting data from cache: $time ms")
            users
        } else {
            val users: List<User>
            val time = measureTimeMillis { users  = clientsDao.getByTrainerId(trainerId, offset, rowCount) }
            println("Getting data from db: $time ms")
            cache[key] = users
            users
        }
    }

    override suspend fun getByLogin(login: String) = clientsDao.getByLogin(login)

    override suspend fun getByEmail(email: String) = clientsDao.getByEmail(email)

    override suspend fun create(item: User) = clientsDao.create(item)

    override suspend fun update(item: User) = when (item.userRole) {
        UserRole.ADMIN -> clientsDao.update(item)
        UserRole.USER -> throw IllegalAccessException()
    }

    override suspend fun delete(item: User) = clientsDao.delete(item)
}